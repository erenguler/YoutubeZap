﻿using Google.Apis.YouTube.v3.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace YoutubeZap.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home

        public ActionResult Index()
        {
            Session["User"] = YoutubeApi.GetUserInfoByAuth();
            ViewBag.Subs = YoutubeApi.GetChannelSubsByAuth();

            return View();
        }
    }
}