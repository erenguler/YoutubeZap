﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace YoutubeZap
{
    public class Channel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }

        public List<Video> Videos { get; set; }

        public Channel()
        {
            Videos = new List<Video>();
        }
    }
}