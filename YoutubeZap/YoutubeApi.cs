﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Upload;
using Google.Apis.Util.Store;
using Google.Apis.YouTube.v3;
using Google.Apis.YouTube.v3.Data;
using System.IO;
using System.Threading;

namespace YoutubeZap
{
    public class YoutubeApi
    {
        private static YouTubeService ytService = Auth();

        // API'ye bağlanma
        private static YouTubeService Auth()
        {
            UserCredential creds;
            using (var stream = new FileStream(HttpContext.Current.Server.MapPath("~/") + "client_secrets.json", FileMode.Open, FileAccess.Read))
            {
                creds = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    new[] { YouTubeService.Scope.Youtube },
                    "user",
                    CancellationToken.None,
                    new FileDataStore("Youtube Zap")
                ).Result;
            }

            var youtubeService = new YouTubeService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = creds,
                ApplicationName = "Youtube Zap"
            });

            return youtubeService;
        }

        // Giriş yapmış kullanıcının bilgilerini veriyor
        public static User GetUserInfoByAuth()
        {
            try
            {
                ChannelsResource.ListRequest channelsListMineRequest = ytService.Channels.List("snippet,contentDetails,statistics");
                channelsListMineRequest.Mine = true;
                ChannelListResponse response = channelsListMineRequest.Execute();

                List<User> users = new List<User>();

                User user = new User();
                user.Id = response.Items[0].Id;
                user.Name = response.Items[0].Snippet.Title;
                user.Image = response.Items[0].Snippet.Thumbnails.Default__.Url;

                return user;
            }
            catch
            {
                return null;
            }
        }

        // Giriş yapmış kullanıcının aboneliklerini veriyor
        public static List<Channel> GetChannelSubsByAuth()
        {
            try
            {
                SubscriptionsResource.ListRequest subscriptionsListMineRequest = ytService.Subscriptions.List("snippet,contentDetails,id");
                subscriptionsListMineRequest.Mine = true;
                subscriptionsListMineRequest.MaxResults = 3;
                SubscriptionListResponse response = subscriptionsListMineRequest.Execute();

                List<Channel> subs = new List<Channel>();

                foreach (var item in response.Items)
                {
                    ChannelsResource.ListRequest channels = ytService.Channels.List("snippet,contentDetails,statistics");
                    channels.Id = item.Snippet.ResourceId.ChannelId;
                    ChannelListResponse channelsResponse = channels.Execute();
                    
                    Channel channel = new Channel();
                    channel.Id = item.Snippet.ResourceId.ChannelId;
                    channel.Name = item.Snippet.Title;
                    channel.Image = item.Snippet.Thumbnails.Default__.Url;

                    string playListId = channelsResponse.Items[0].ContentDetails.RelatedPlaylists.Uploads;
                    PlaylistItemsResource.ListRequest playlists = ytService.PlaylistItems.List("snippet,contentDetails");
                    playlists.PlaylistId = playListId;
                    playlists.MaxResults = 6;

                    PlaylistItemListResponse resp = playlists.Execute();
                    foreach (var item2 in resp.Items)
                    {
                        Video video = new Video();
                        video.Id = item2.Snippet.ResourceId.VideoId;
                        video.Name = item2.Snippet.Title;
                        video.Image = item2.Snippet.Thumbnails.Default__.Url;

                        channel.Videos.Add(video);
                    }
                    subs.Add(channel);
                }
                return subs;
            }
            catch
            {
                return null;
            }
        }

        // Kanal ID'si verilen kullanıcının aboneliklerini veriyor
        public static SubscriptionListResponse GetSubsByChannelId(string channelId)
        {
            try
            {
                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters.Add("part", "snippet,contentDetails");
                parameters.Add("channelId", channelId);
                SubscriptionsResource.ListRequest subscriptionsListByChannelIdRequest = ytService.Subscriptions.List(parameters["part"].ToString());
                if (parameters.ContainsKey("channelId") && parameters["channelId"] != "")
                {
                    subscriptionsListByChannelIdRequest.ChannelId = parameters["channelId"].ToString();
                }
                subscriptionsListByChannelIdRequest.MaxResults = 50;
                SubscriptionListResponse response = subscriptionsListByChannelIdRequest.Execute();
                return response;
            }
            catch
            {
                return null;
            }
        }

    }
}